
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace api.tnhs.dangky.Common
{
    public class ChuanHoa
    {
        static Dictionary<string, string> dictChar;
        static char[,] vowelTable = {
            {'a', 'à', 'á', 'ả', 'ã', 'ạ'},
            {'ă', 'ằ', 'ắ', 'ẳ', 'ẵ', 'ặ'},
            {'â', 'ầ', 'ấ', 'ẩ', 'ẫ', 'ậ'},
            {'e', 'è', 'é', 'ẻ', 'ẽ', 'ẹ'},
            {'ê', 'ề', 'ế', 'ể', 'ễ', 'ệ'},
            {'i', 'ì', 'í', 'ỉ', 'ĩ', 'ị'},
            {'o', 'ò', 'ó', 'ỏ', 'õ', 'ọ'},
            {'ô', 'ồ', 'ố', 'ổ', 'ỗ', 'ộ'},
            {'ơ', 'ờ', 'ớ', 'ở', 'ỡ', 'ợ'},
            {'u', 'ù', 'ú', 'ủ', 'ũ', 'ụ'},
            {'ư', 'ừ', 'ứ', 'ử', 'ữ', 'ự'},
            {'y', 'ỳ', 'ý', 'ỷ', 'ỹ', 'ỵ'}
    };
        static string[] vietnamChars = new string[] {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n",
                "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I",
                "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "à", "á", "ả", "ã",
                "ạ", "ầ", "ấ", "ẩ", "ẫ", "ậ", "ằ", "ắ", "ẳ", "ẵ", "ặ", "è", "é", "ẻ", "ẽ", "ẹ", "ề", "ế", "ể", "ễ", "ệ",
                "ì", "í", "ỉ", "ĩ", "ị", "ò", "ó", "ỏ", "õ", "ọ", "ô", "ồ", "ố", "ổ", "ỗ", "ộ", "ờ", "ớ", "ở", "ỡ", "ợ", "ù",
                "ú", "ủ", "ũ", "ụ", "ừ", "ứ", "ử", "ữ", "ự", "ỳ", "ý", "ỷ", "ỹ", "ỵ", "À", "Á", "Ả", "Ã", "Ạ", "Ầ", "Ấ",
                "Ẩ", "Ẫ", "Ậ", "Ằ", "Ắ", "Ẳ", "Ẵ", "Ặ", "È", "É", "Ẻ", "Ẽ", "Ẹ", "Ề", "Ế", "Ể", "Ễ", "Ệ", "Ì", "Í", "Ỉ",
                "Ĩ", "Ị", "Ò", "Ó", "Ỏ", "Õ", "Ọ", "Ô", "Ồ", "Ố", "Ổ", "Ỗ", "Ộ", "Ờ", "Ớ", "Ở", "Ỡ", "Ợ", "Ù", "Ú", "Ủ", "Ũ",
                "Ụ", "Ừ", "Ứ", "Ử", "Ữ", "Ự", "Ỳ", "Ý", "Ỷ", "Ỹ", "Ỵ", "đ", "Đ", "ă", "Ă", "â", "Â", "ê", "Ê", "ô", "Ô", "ơ", "Ơ", "ư", "Ư"};
        static Dictionary<char, int> vowelLookupRow = new Dictionary<char, int>();
        static Dictionary<char, int> vowelLookupColumn = new Dictionary<char, int>();


        public ChuanHoa()
        {
            dictChar = loadDictChar();
        }

        private static Dictionary<String, String> loadDictChar()
        {
            string textchar1252 = "à|á|ả|ã|ạ|ầ|ấ|ẩ|ẫ|ậ|ằ|ắ|ẳ|ẵ|ặ|è|é|ẻ|ẽ|ẹ|ề|ế|ể|ễ|ệ|ì|í|ỉ|ĩ|ị|ò|ó|ỏ|õ|ọ|ồ|ố|ổ|ỗ|ộ|ờ|ớ|ở|ỡ|ợ|ù|" +
                    "ú|ủ|ũ|ụ|ừ|ứ|ử|ữ|ự|ỳ|ý|ỷ|ỹ|ỵ|À|Á|Ả|Ã|Ạ|Ầ|Ấ|Ẩ|Ẫ|Ậ|Ằ|Ắ|Ẳ|Ẵ|Ặ|È|É|Ẻ|Ẽ|Ẹ|Ề|Ế|Ể|Ễ|Ệ|Ì|Í|Ỉ|Ĩ|Ị|Ò|Ó|Ỏ|Õ|Ọ|Ồ|Ố|Ổ|Ỗ" +
                    "|Ộ|Ờ|Ớ|Ở|Ỡ|Ợ|Ù|Ú|Ủ|Ũ|Ụ|Ừ|Ứ|Ử|Ữ|Ự|Ỳ|Ý|Ỷ|Ỹ|Ỵ|Ð";
            String[] char1252 = Regex.Split(textchar1252, "\\|");
            string textcharUTF8 = "à|á|ả|ã|ạ|ầ|ấ|ẩ|ẫ|ậ|ằ|ắ|ẳ|ẵ|ặ|è|é|ẻ|ẽ|ẹ|ề|ế|ể|ễ|ệ|ì|í|ỉ|ĩ|ị|ò|ó|ỏ|õ|ọ|ồ|ố|ổ|ỗ|ộ|ờ|ớ|ở|ỡ|ợ|ù|ú|" +
                    "ủ|ũ|ụ|ừ|ứ|ử|ữ|ự|ỳ|ý|ỷ|ỹ|ỵ|À|Á|Ả|Ã|Ạ|Ầ|Ấ|Ẩ|Ẫ|Ậ|Ằ|Ắ|Ẳ|Ẵ|Ặ|È|É|Ẻ|Ẽ|Ẹ|Ề|Ế|Ể|Ễ|Ệ|Ì|Í|Ỉ|Ĩ|Ị|Ò|Ó|Ỏ|Õ|Ọ|Ồ|Ố|Ổ|Ỗ|" +
                    "Ộ|Ờ|Ớ|Ở|Ỡ|Ợ|Ù|Ú|Ủ|Ũ|Ụ|Ừ|Ứ|Ử|Ữ|Ự|Ỳ|Ý|Ỷ|Ỹ|Ỵ|Đ";
            String[] charUTF8 = Regex.Split(textcharUTF8, "\\|");

            Dictionary<String, String> dictChar = new Dictionary<String, String>();
            for (int i = 0; i < char1252.Length; i++)
            {
                dictChar.Add(char1252[i], charUTF8[i]);
            }
            return dictChar;
        }

        public static string chuan_hoa_dau_cau_tieng_viet(string text)
        {
            var words = Regex.Split(text, "\\s+");
            if (vowelLookupRow.Count == 0 || vowelLookupColumn.Count == 0)
            {
                for (int i = 0; i < vowelTable.GetLength(0); i++)
                {
                    for (int j = 0; j < vowelTable.GetLength(1); j++)
                    {
                        vowelLookupRow.Add(vowelTable[i, j], i);
                        vowelLookupColumn.Add(vowelTable[i, j], j);
                    }
                }
            }
            for (int i = 0; i < words.Length; i++)
            {
                List<Boolean> uppers = getUpperState(words[i]);
                try
                {
                    words[i] = updateUpperState(correctVnAccentWord(words[i]), uppers);
                }
                catch (Exception e)
                {

                }
            }
            return string.Join(" ", words);
        }

        private static List<Boolean> getUpperState(String word)
        {
            var uppers = new List<Boolean>();
            char[] chars = word.ToCharArray();
            for (int i = 0; i < chars.Length; i++)
            {
                uppers.Add(Char.IsUpper(chars[i]));
            }
            return uppers;
        }

        private static String updateUpperState(String word, List<Boolean> uppers)
        {
            char[] chars = word.ToCharArray();
            for (int i = 0; i < chars.Length; i++)
            {
                chars[i] = uppers[i] ? Char.ToUpper(chars[i]) : chars[i];
            }
            return new string(chars);
        }

        private static bool isVietnamWord(string word)
        {
            /*
             * Kiểm tra có phải là từ tiếng việt, có dấu
             * Input word cần lowerCase nhé
             * */
            bool hasAccent = false;
            int currentVowel = -1;
            for (int i = 0; i < word.Length; i++)
            {
                var test = word[i];
                if (!vietnamChars.Contains(word[i].ToString()))
                {
                    return false;
                }
                if (vowelLookupRow.ContainsKey(word[i]))
                {
                    if (currentVowel == -1)
                        currentVowel = i;
                    else
                    {
                        if (i - currentVowel != 1) return false;
                        currentVowel = i;
                    }
                    var word1 = word[1];
                    if (vowelLookupColumn[word[i]] > 0)
                    {
                        if (hasAccent) return false; // Một từ có hai thanh dấu
                        hasAccent = true;
                    }
                }
            }
            return hasAccent;
        }

        private static String correctVnAccentWord(String word)
        {
            // Tách head tail char
            if (!(Regex.Match(word, ".*\\p{L}+.*")).Success)
            {
                return word;
            }
            word = Regex.Replace(word, @"^([^\\p{L}]*)([\\p{L}]+)([^\\p{L}]*)$", "$1 $2 $3").Trim();
            String[] parts = Regex.Split(word, @"\\s+");
            String head = "", tWord, tail = "";
            if (parts.Length == 1)
            {
                word = parts[0];
            }
            else if (parts.Length == 2)
            {
                if (Regex.IsMatch(parts[0], @"\\p{L}+"))
                {
                    word = parts[0];
                    tail = parts[1];
                }
                else
                {
                    head = parts[0];
                    word = parts[1];
                }
            }
            else
            {
                head = parts[0];
                word = parts[1];
                tail = parts[2];
            }
            word = word.ToLower();
            if (!isVietnamWord(word)) {
                return head + word + tail;
            }

            char[] chars = word.ToCharArray();
            int accentPosition = 0, x, y;
            bool isQuOrGi = false;

            List<int> vowelsIndex = new List<int>();
            for (int i = 0; i < chars.Length; i++)
            {
                if (vowelLookupRow.ContainsKey(chars[i]) && vowelLookupColumn.ContainsKey(chars[i]))
                {

                    x = vowelLookupRow[chars[i]];
                    y = vowelLookupColumn[chars[i]];

                    if (x == -1) continue;
                    else if (x == 9)
                    { // qu
                        if (i != 0 && chars[i - 1] == 'q')
                        {
                            chars[i] = 'u';
                            isQuOrGi = true;
                        }
                    }
                    else if (x == 5)
                    { // gi
                        if (i != 0 && chars[i - 1] == 'g')
                        {
                            chars[i] = 'i';
                            isQuOrGi = true;
                        }
                    }
                    if (y != 0)
                    {
                        accentPosition = y;
                        chars[i] = vowelTable[x, 0];
                    }
                    if (!isQuOrGi || i != 1)
                    {
                        vowelsIndex.Add(i);
                    }
                }
            }
            if (vowelsIndex.Count < 2)
            {
                if (isQuOrGi)
                {
                    if (chars.Length == 2)
                    {
                        x = vowelLookupRow[chars[1]];
                        chars[1] = vowelTable[x, accentPosition];
                    }
                    else
                    {
                        x = vowelLookupRow[chars[2]];
                        if (x != -1)
                        {
                            chars[2] = vowelTable[x, accentPosition];
                        }
                        else
                        {
                            chars[1] = (chars[1] == 'i' ? vowelTable[5,accentPosition] : vowelTable[9, accentPosition]);
                        }
                    }
                    return head + new string(chars) + tail;
                }
                return head + word + tail;
            }
            foreach (var index in vowelsIndex)
            {
                x = vowelLookupRow[chars[index]];
                if (x == 4 || x == 8)
                { // ê, ơ
                    chars[index] = vowelTable[x, accentPosition];
                    return head + new string(chars) + tail;
                }
            }
            if (vowelsIndex.Count == 2)
            {
                if (vowelsIndex[vowelsIndex.Count - 1] == chars.Length - 1)
                {
                    x = vowelLookupRow[chars[vowelsIndex[0]]];
                    chars[vowelsIndex[0]] = vowelTable[x, accentPosition];
                }
                else
                {
                    x = vowelLookupRow[chars[vowelsIndex[1]]];
                    chars[vowelsIndex[1]] = vowelTable[x, accentPosition];
                }
            }
            else
            {
                x = vowelLookupRow[chars[vowelsIndex[1]]];
                chars[vowelsIndex[1]] = vowelTable[x, accentPosition];
            }
            return head + new string(chars) + tail;
        }
    }
}
